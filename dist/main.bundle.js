webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-core-layout></app-core-layout>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_tabview__ = __webpack_require__("./node_modules/primeng/tabview.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_tabview___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_tabview__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_inputtext__ = __webpack_require__("./node_modules/primeng/inputtext.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_inputtext___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_inputtext__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_dropdown__ = __webpack_require__("./node_modules/primeng/dropdown.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_dropdown___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_dropdown__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_button__ = __webpack_require__("./node_modules/primeng/button.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_button___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_primeng_button__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_panel__ = __webpack_require__("./node_modules/primeng/panel.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_panel___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_primeng_panel__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_primeng_datatable__ = __webpack_require__("./node_modules/primeng/datatable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_primeng_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_primeng_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__core_layout_core_layout_component__ = __webpack_require__("./src/app/core-layout/core-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_layout_header_header_component__ = __webpack_require__("./src/app/core-layout/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__main_main_component__ = __webpack_require__("./src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__main_customer_list_customer_list_component__ = __webpack_require__("./src/app/main/customer-list/customer-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__main_customer_details_customer_details_component__ = __webpack_require__("./src/app/main/customer-details/customer-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__customer_service__ = __webpack_require__("./src/app/customer.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__core_layout_core_layout_component__["a" /* CoreLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_12__core_layout_header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_13__main_main_component__["a" /* MainComponent */],
                __WEBPACK_IMPORTED_MODULE_14__main_customer_list_customer_list_component__["a" /* CustomerListComponent */],
                __WEBPACK_IMPORTED_MODULE_15__main_customer_details_customer_details_component__["a" /* CustomerDetailsComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4_primeng_tabview__["TabViewModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_inputtext__["InputTextModule"],
                __WEBPACK_IMPORTED_MODULE_6_primeng_dropdown__["DropdownModule"],
                __WEBPACK_IMPORTED_MODULE_7_primeng_button__["ButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_panel__["PanelModule"],
                __WEBPACK_IMPORTED_MODULE_9_primeng_datatable__["DataTableModule"]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_16__customer_service__["a" /* CustomerService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core-layout/core-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"core-layout\">\n  <app-header></app-header>\n  <div class=\"core-layout__main\">\n  \t<app-main></app-main>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/core-layout/core-layout.component.scss":
/***/ (function(module, exports) {

module.exports = ".core-layout {\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  .core-layout__main {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    overflow: hidden; }\n"

/***/ }),

/***/ "./src/app/core-layout/core-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CoreLayoutComponent = /** @class */ (function () {
    function CoreLayoutComponent() {
    }
    CoreLayoutComponent.prototype.ngOnInit = function () {
    };
    CoreLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-core-layout',
            template: __webpack_require__("./src/app/core-layout/core-layout.component.html"),
            styles: [__webpack_require__("./src/app/core-layout/core-layout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CoreLayoutComponent);
    return CoreLayoutComponent;
}());



/***/ }),

/***/ "./src/app/core-layout/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\n  <div class=\"header__title\">\n    PrimeNg Example\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/core-layout/header/header.component.scss":
/***/ (function(module, exports) {

module.exports = ".header {\n  width: 100%;\n  background-color: #1976d2;\n  color: #ffffff;\n  height: 3.125rem;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.3);\n          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.3); }\n  .header__title {\n    margin: 0 1rem 0 1rem;\n    font-size: 1.5rem; }\n"

/***/ }),

/***/ "./src/app/core-layout/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-header',
            template: __webpack_require__("./src/app/core-layout/header/header.component.html"),
            styles: [__webpack_require__("./src/app/core-layout/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/customer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_customers__ = __webpack_require__("./src/app/mock-customers.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomerService = /** @class */ (function () {
    function CustomerService() {
        this.customers = __WEBPACK_IMPORTED_MODULE_1__mock_customers__["a" /* CUSTOMERS */];
    }
    CustomerService.prototype.getCustomers = function () {
        return this.customers;
    };
    CustomerService.prototype.addCustomer = function (customer) {
        this.customers.push(customer);
    };
    CustomerService.prototype.removeCustomer = function (i) {
        this.customers.splice(i, 1);
    };
    CustomerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], CustomerService);
    return CustomerService;
}());



/***/ }),

/***/ "./src/app/customer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Customer; });
var Customer = /** @class */ (function () {
    function Customer() {
    }
    return Customer;
}());



/***/ }),

/***/ "./src/app/main/customer-details/customer-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"customer-details\">\n  <div *ngIf=\"!customer\" class=\"customer-details__message\">\n    Select account to view and edit its details.\n  </div>\n  <p-tabView *ngIf=\"customer\">\n    <p-tabPanel header=\"General\">\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <div class=\"ui-inputgroup\">\n            <span class=\"ui-inputgroup-addon\"><i class=\"fa fa-building\"></i></span>\n            <input type=\"text\" pInputText placeholder=\"Customer Name\" [(ngModel)]=\"customer.name\">         \n          </div>\n        </div>\n        <div class=\"ui-g-12 ui-md-4\">\n          <div class=\"ui-inputgroup\">\n            <span class=\"ui-inputgroup-addon\"><i class=\"fa fa-id-card\"></i></span>\n            <input type=\"text\" pInputText placeholder=\"Account Code\" [(ngModel)]=\"customer.accountCode\"> \n          </div>\n        </div>\n      </div>\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <input type=\"text\" pInputText placeholder=\"Tax Number\" [(ngModel)]=\"customer.taxNumber\"> \n        </div>\n      </div>\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <input type=\"text\" pInputText placeholder=\"Tax Rule\" [(ngModel)]=\"customer.taxRule\"> \n        </div>\n      </div>\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <p-dropdown [options]=\"currencies\" [(ngModel)]=\"customer.currency\" placeholder=\"Currency\" [autoWidth]=\"false\"></p-dropdown>\n        </div>\n      </div>\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <p-dropdown [options]=\"terms\" [(ngModel)]=\"customer.paymentTerms\" placeholder=\"Payment Terms\" [autoWidth]=\"false\"></p-dropdown>\n        </div>\n        <div class=\"ui-g-12 ui-md-4\">\n          <p-dropdown [options]=\"terms\" [(ngModel)]=\"customer.shippingTerms\" placeholder=\"Shipping Terms\" [autoWidth]=\"false\"></p-dropdown>\n        </div>\n      </div>\n      <div class=\"ui-g ui-fluid\">\n        <div class=\"ui-g-12 ui-md-4\">\n          <input type=\"text\" pInputText placeholder=\"Credit Limit\" [(ngModel)]=\"customer.creditLimit\"> \n        </div>\n      </div>\n      <div class=\"customer-details__margin-top\">\n        <p-panel>\n          <p-header>\n            <div class=\"customer-details__header\">\n              <div class=\"customer-details__header__title\">Phone Numbers</div>\n              <button pButton icon=\"fa fa-fw fa-plus\" (click)=\"addPhone()\" title=\"Add Phone Number\"></button>\n            </div>\n          </p-header>\n          <div class=\"ui-g\" *ngFor=\"let phone of customer.phones; let i=index;\">\n            <div class=\"ui-g-10 ui-md-4 ui-fluid\">\n              <div class=\"ui-inputgroup\">\n                <span class=\"ui-inputgroup-addon\"><i class=\"fa fa-phone\"></i></span>\n                <input type=\"text\" pInputText placeholder=\"Phone Number\" [(ngModel)]=\"phone.number\">\n              </div>\n            </div>\n            <div class=\"ui-g-2 ui-md-1\">\n              <button class=\"ui-button-danger\" pButton icon=\"fa fa-fw fa-minus\" (click)=\"removePhone(i)\" title=\"Remove Phone Number\">\n              </button>\n            </div>\n          </div>\n        </p-panel>\n      </div>\n      <div class=\"customer-details__margin-top\">\n        <p-panel>\n          <p-header>\n            <div class=\"customer-details__header\">\n              <div class=\"customer-details__header__title\">Addresses</div>\n              <button pButton icon=\"fa fa-fw fa-plus\" (click)=\"addAddress()\" title=\"Add Address\"></button>\n            </div>\n          </p-header>\n          <div *ngFor=\"let address of customer.addresses; let i=index;\">\n            <div class=\"ui-g\">\n              <div class=\"ui-g-10 ui-md-4 ui-fluid\">\n                <div class=\"ui-inputgroup\">\n                  <span class=\"ui-inputgroup-addon\"><i class=\"fa fa-map-marker\"></i></span>\n                  <input type=\"text\" pInputText placeholder=\"Line 1\" [(ngModel)]=\"address.line1\">         \n                </div>\n              </div>\n              <div class=\"ui-g-2 ui-md-1\">\n                <button class=\"ui-button-danger\" pButton icon=\"fa fa-fw fa-minus\" (click)=\"removeAddress(i)\"title=\"Remove Address\">\n                </button>\n              </div>\n            </div>\n            <div class=\"ui-g ui-fluid\">\n              <div class=\"ui-g-12 ui-md-4 ui-fluid\">\n                <input type=\"text\" pInputText placeholder=\"Line 2\" [(ngModel)]=\"address.line2\">  \n              </div>\n            </div>\n            <div class=\"ui-g ui-fluid\">\n              <div class=\"ui-g-12 ui-md-4\">\n                <input type=\"text\" pInputText placeholder=\"City\" [(ngModel)]=\"address.city\">  \n              </div>\n              <div class=\"ui-g-12 ui-md-4\">\n                <input type=\"text\" pInputText placeholder=\"State / Province\" [(ngModel)]=\"address.state\">  \n              </div>\n            </div>\n            <div class=\"ui-g ui-fluid\">\n              <div class=\"ui-g-12 ui-md-4\">\n                <input type=\"text\" pInputText placeholder=\"Country\" [(ngModel)]=\"address.country\">  \n              </div>\n              <div class=\"ui-g-12 ui-md-4\">\n                <input type=\"text\" pInputText placeholder=\"Zip / Postal Code\" [(ngModel)]=\"address.zip\">  \n              </div>\n            </div>\n          </div>\n        </p-panel>\n      </div>\n    </p-tabPanel>\n    <p-tabPanel header=\"Contacts\">\n      <p-dataTable [value]=\"customer.contacts\" [responsive]=\"true\" [resizableColumns]=\"true\">\n        <p-column field=\"firstName\" header=\"First Name\"></p-column>\n        <p-column field=\"lastName\" header=\"Last Name\"></p-column>\n        <p-column field=\"title\" header=\"Title\"></p-column>\n        <p-column field=\"department\" header=\"Department\"></p-column>\n        <p-column field=\"email\" header=\"Email\"></p-column>\n        <p-column field=\"phone\" header=\"Phone\"></p-column>\n      </p-dataTable>\n    </p-tabPanel>\n    <p-tabPanel header=\"Notes\">\n      <p-dataTable [value]=\"customer.notes\" [responsive]=\"true\">\n        <p-column field=\"date\" header=\"Date\"></p-column>\n        <p-column field=\"notes\" header=\"Notes\"></p-column>\n      </p-dataTable>\n    </p-tabPanel>\n  </p-tabView> \n</div>\n"

/***/ }),

/***/ "./src/app/main/customer-details/customer-details.component.scss":
/***/ (function(module, exports) {

module.exports = ".customer-details {\n  width: 100%;\n  max-width: 56.25rem;\n  margin: 0.5rem auto; }\n  .customer-details__message {\n    margin: 2rem; }\n  .customer-details__margin-top {\n    margin-top: 2rem; }\n  .customer-details__header {\n    width: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n  .customer-details__header__title {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center; }\n"

/***/ }),

/***/ "./src/app/main/customer-details/customer-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__customer__ = __webpack_require__("./src/app/customer.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomerDetailsComponent = /** @class */ (function () {
    function CustomerDetailsComponent() {
        this.currencies = [{ label: 'CAD', value: 'cad' }, { label: 'USD', value: 'usd' }];
        this.terms = [
            { label: '10 days', value: 10 },
            { label: '15 days', value: 15 },
            { label: '30 days', value: 30 },
            { label: '30 days', value: 60 }
        ];
    }
    CustomerDetailsComponent.prototype.addPhone = function () {
        if (!this.customer.phones) {
            this.customer.phones = [];
        }
        this.customer.phones.push({ number: '' });
    };
    CustomerDetailsComponent.prototype.removePhone = function (index) {
        if (this.customer.phones) {
            this.customer.phones.splice(index, 1);
        }
    };
    CustomerDetailsComponent.prototype.addAddress = function () {
        if (!this.customer.addresses) {
            this.customer.addresses = [];
        }
        this.customer.addresses.push({ line1: '', line2: '', city: '', state: '', country: '', zip: '' });
    };
    CustomerDetailsComponent.prototype.removeAddress = function (index) {
        if (this.customer.addresses) {
            this.customer.addresses.splice(index, 1);
        }
    };
    CustomerDetailsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__customer__["a" /* Customer */])
    ], CustomerDetailsComponent.prototype, "customer", void 0);
    CustomerDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-customer-details',
            template: __webpack_require__("./src/app/main/customer-details/customer-details.component.html"),
            styles: [__webpack_require__("./src/app/main/customer-details/customer-details.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CustomerDetailsComponent);
    return CustomerDetailsComponent;
}());



/***/ }),

/***/ "./src/app/main/customer-list/customer-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"customer-list\">\n  <div class=\"customer-list__buttons\">\n    <button class=\"ui-button-secondary\" pButton icon=\"fa fa-fw fa-plus\" (click)=\"addCustomer()\" title=\"Add Account\"></button>\n  </div>\n  <div class=\"customer-list__body\">\n\t  <div [ngClass]=\"selectedCustomer === customer? \n\t  \t'customer-list__body__item customer-list__body__item--active':\n\t  \t'customer-list__body__item'\" \n\t    *ngFor=\"let customer of customers; let i=index;\" \n\t    (click)=\"selectCustomer(null, customer)\">\n\t    <a href=\"\" (click)=\"selectCustomer($event, customer)\">\n\t    \t{{ customer.name }}\n\t    </a>\n\t    <button class=\"ui-button-secondary\" pButton icon=\"fa fa-fw fa-minus\" (click)=\"removeCustomer(i)\" title=\"Remove Account\">\n\t    </button>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/main/customer-list/customer-list.component.scss":
/***/ (function(module, exports) {

module.exports = ".customer-list {\n  height: 100%;\n  background-color: #f5f7f8;\n  overflow-y: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  .customer-list__buttons {\n    height: 2.25rem;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 0.5rem; }\n  .customer-list__body {\n    overflow-y: auto;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1; }\n  .customer-list__body__item {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      background-color: #f5f7f8;\n      padding: 1rem;\n      cursor: pointer; }\n  .customer-list__body__item a {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        text-decoration: none;\n        color: inherit; }\n  .customer-list__body__item--active {\n        background-color: #1976d2;\n        color: #ffffff; }\n  .customer-list__body__item:focus {\n        outline: none;\n        border-color: #1976d2;\n        border-style: solid;\n        border-width: 1px; }\n"

/***/ }),

/***/ "./src/app/main/customer-list/customer-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__customer_service__ = __webpack_require__("./src/app/customer.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomerListComponent = /** @class */ (function () {
    function CustomerListComponent(customerService) {
        this.customerService = customerService;
        this.select = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    CustomerListComponent.prototype.ngOnInit = function () {
        this.customers = this.customerService.getCustomers();
        this.selectedCustomer = this.customers[0];
        this.select.emit(this.selectedCustomer);
    };
    CustomerListComponent.prototype.addCustomer = function () {
        var newCustomer = { name: 'New Account' };
        this.customerService.addCustomer(newCustomer);
        this.selectedCustomer = newCustomer;
        this.select.emit(newCustomer);
    };
    CustomerListComponent.prototype.removeCustomer = function (i) {
        this.customerService.removeCustomer(i);
        this.selectedCustomer = null;
        this.select.emit(null);
    };
    CustomerListComponent.prototype.selectCustomer = function (event, customer) {
        if (event) {
            event.preventDefault();
        }
        this.selectedCustomer = customer;
        this.select.emit(customer);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], CustomerListComponent.prototype, "select", void 0);
    CustomerListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-customer-list',
            template: __webpack_require__("./src/app/main/customer-list/customer-list.component.html"),
            styles: [__webpack_require__("./src/app/main/customer-list/customer-list.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__customer_service__["a" /* CustomerService */]])
    ], CustomerListComponent);
    return CustomerListComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main\">\n  <div class=\"main__sm-menu\">\n    <button class=\"ui-button-secondary\" pButton icon=\"fa fa-fw fa-users\" (click)=\"expandList()\"></button>\n  </div>\n  <div class=\"main__body\">\n    <div [ngClass]=\"listExpanded ? 'main__body__list main__body__list--active':'main__body__list'\">\n      <app-customer-list (select)=\"selectCustomer($event)\"></app-customer-list>\n    </div>\n    <div class=\"main__body__details\">\n      <app-customer-details [customer]=\"selectedCustomer\"></app-customer-details>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/main/main.component.scss":
/***/ (function(module, exports) {

module.exports = ".main {\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  .main__sm-menu {\n    display: none; }\n  .main__body {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    position: relative; }\n  .main__body__list {\n      width: 16rem;\n      -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.3);\n              box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.3);\n      -webkit-transition: all 0.3s;\n      transition: all 0.3s;\n      z-index: 99; }\n  .main__body__details {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      overflow-y: auto; }\n  @media screen and (max-width: 40em) {\n  .main__sm-menu {\n    height: 2.25rem;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 0.5rem;\n    background-color: #f5f7f8; }\n  .main__body__list {\n    position: absolute;\n    left: -16rem;\n    top: 0;\n    bottom: 0; }\n    .main__body__list--active {\n      left: 0; } }\n"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainComponent = /** @class */ (function () {
    function MainComponent() {
        this.listExpanded = false;
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent.prototype.expandList = function () {
        this.listExpanded = !this.listExpanded;
    };
    MainComponent.prototype.selectCustomer = function (customer) {
        this.selectedCustomer = customer;
        this.listExpanded = false;
    };
    MainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main',
            template: __webpack_require__("./src/app/main/main.component.html"),
            styles: [__webpack_require__("./src/app/main/main.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/mock-customers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CUSTOMERS; });
var CUSTOMERS = [
    {
        name: 'Fabricland',
        contacts: [
            { firstName: 'John', lastName: 'Doe',
                title: 'Sales Manager', department: 'Sales',
                email: 'jdoe@frabricland.ca', phone: '(416)123-4567'
            },
            { firstName: 'Jane', lastName: 'Smith',
                title: 'Customer Support', department: 'Support',
                email: 'jsmith@frabricland.ca', phone: '(416)123-9012'
            },
            { firstName: 'Joe', lastName: 'Matthews',
                title: 'Customer Support', department: 'Support',
                email: 'jmatthews@frabricland.ca', phone: '(416)345-9012'
            }
        ],
        notes: [
            { date: '3/8/2018', notes: 'Hosted call to discuss installation of new widget at new store in Oakville.' },
            { date: '3/8/2018', notes: 'Hosted call to discuss installation of new widget at new store in Milton.' }
        ]
    },
    {
        name: 'Kawartha',
        contacts: [
            { firstName: 'Peter', lastName: 'Chan',
                title: 'Sales Manager', department: 'Sales',
                email: 'pchan@kawrtha.ca', phone: '(416)121-4567'
            },
            { firstName: 'Bill', lastName: 'Jones',
                title: 'Customer Support', department: 'Support',
                email: 'bjones@kawrtha.ca', phone: '(416)123-8934'
            },
            { firstName: 'Catherine', lastName: 'Brown',
                title: 'Customer Support', department: 'Support',
                email: 'cbrown@kawrtha.ca', phone: '(416)345-1256'
            }
        ],
        notes: [
            { date: '3/7/2018', notes: 'Meeting to discuss requirements of new product they are launching.' }
        ]
    }
];


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map