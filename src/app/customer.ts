export class Customer {
  name: string;
  accountCode?: string;
  taxNumber?: string;
  currency?: string;
  paymentTerms?: string;
  shippingTerms?: string;
  creditLimit?: string;
  phones?: any[];
  addresses?: any[];
  contacts?: any[];
  notes?: any[];
}
