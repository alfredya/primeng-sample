import { Injectable } from '@angular/core';
import { Customer } from './customer';
import { CUSTOMERS } from './mock-customers';

@Injectable()
export class CustomerService {

  customers: Customer[];

  constructor() {
    this.customers = CUSTOMERS;
  }

  getCustomers(): Customer[] {
    return this.customers;
  }

  addCustomer(customer: Customer): void {
    this.customers.push(customer);
  }

  removeCustomer(i: number): void {
    this.customers.splice(i, 1);
  }
}
