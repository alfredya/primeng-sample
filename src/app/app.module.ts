import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { DataTableModule } from 'primeng/datatable';

import { AppComponent } from './app.component';
import { CoreLayoutComponent } from './core-layout/core-layout.component';
import { HeaderComponent } from './core-layout/header/header.component';
import { MainComponent } from './main/main.component';
import { CustomerListComponent } from './main/customer-list/customer-list.component';
import { CustomerDetailsComponent } from './main/customer-details/customer-details.component';

import { CustomerService } from './customer.service';


@NgModule({
  declarations: [
    AppComponent,
    CoreLayoutComponent,
    HeaderComponent,
    MainComponent,
    CustomerListComponent,
    CustomerDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    TabViewModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    PanelModule,
    DataTableModule
  ],
  providers: [
    CustomerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
