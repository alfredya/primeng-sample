export const CUSTOMERS = [
  {
    name: 'Fabricland',
    contacts: [
      { firstName: 'John', lastName: 'Doe',
        title: 'Sales Manager', department: 'Sales',
        email: 'jdoe@frabricland.ca', phone: '(416)123-4567'
      },
      { firstName: 'Jane', lastName: 'Smith',
        title: 'Customer Support', department: 'Support',
        email: 'jsmith@frabricland.ca', phone: '(416)123-9012'
      },
      { firstName: 'Joe', lastName: 'Matthews',
        title: 'Customer Support', department: 'Support',
        email: 'jmatthews@frabricland.ca', phone: '(416)345-9012'
      }
    ],
    notes: [
      { date: '3/8/2018', notes: 'Hosted call to discuss installation of new widget at new store in Oakville.' },
      { date: '3/8/2018', notes: 'Hosted call to discuss installation of new widget at new store in Milton.' }
    ]
  },
  {
    name: 'Kawartha',
    contacts: [
      { firstName: 'Peter', lastName: 'Chan',
        title: 'Sales Manager', department: 'Sales',
        email: 'pchan@kawrtha.ca', phone: '(416)121-4567'
      },
      { firstName: 'Bill', lastName: 'Jones',
        title: 'Customer Support', department: 'Support',
        email: 'bjones@kawrtha.ca', phone: '(416)123-8934'
      },
      { firstName: 'Catherine', lastName: 'Brown',
        title: 'Customer Support', department: 'Support',
        email: 'cbrown@kawrtha.ca', phone: '(416)345-1256'
      }
    ],
    notes: [
      { date: '3/7/2018', notes: 'Meeting to discuss requirements of new product they are launching.' }
    ]
  }
];
