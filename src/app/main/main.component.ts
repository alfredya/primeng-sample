import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  selectedCustomer: Customer;
  listExpanded = false;

  constructor() { }

  ngOnInit() {
  }

  expandList() {
    this.listExpanded = !this.listExpanded;
  }

  selectCustomer(customer: Customer): void {
    this.selectedCustomer = customer;
    this.listExpanded = false;
  }
}
