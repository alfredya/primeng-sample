import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Customer } from '../../customer';
import { CustomerService } from '../../customer.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  @Output() select: EventEmitter<Customer> = new EventEmitter<Customer>();

  customers: Customer[];
  selectedCustomer: Customer;

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.customers = this.customerService.getCustomers();
    this.selectedCustomer = this.customers[0];
    this.select.emit(this.selectedCustomer);
  }

  addCustomer(): void {
    const newCustomer = { name: 'New Account' };
    this.customerService.addCustomer(newCustomer);
    this.selectedCustomer = newCustomer;
    this.select.emit(newCustomer);
  }

  removeCustomer(i): void {
    this.customerService.removeCustomer(i);
    this.selectedCustomer = null;
    this.select.emit(null);
  }

  selectCustomer(event, customer: Customer): void {
    if (event) {
      event.preventDefault();
    }
    this.selectedCustomer = customer;
    this.select.emit(customer);
  }
}
