import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../../customer';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
  @Input() customer: Customer;

  currencies: any[];
  terms: any[];

  constructor() {
    this.currencies = [{label: 'CAD', value: 'cad'}, {label: 'USD', value: 'usd'}];
    this.terms = [
      {label: '10 days', value: 10},
      {label: '15 days', value: 15},
      {label: '30 days', value: 30},
      {label: '30 days', value: 60}
    ];
  }

  addPhone(): void {
    if (!this.customer.phones) {
      this.customer.phones = [];
    }
    this.customer.phones.push({ number: '' });
  }

  removePhone(index: number): void {
    if (this.customer.phones) {
      this.customer.phones.splice(index, 1);
    }
  }

  addAddress(): void {
    if (!this.customer.addresses) {
      this.customer.addresses = [];
    }
    this.customer.addresses.push({ line1: '', line2: '', city: '', state: '', country: '', zip: '' });
  }

  removeAddress(index: number): void {
    if (this.customer.addresses) {
      this.customer.addresses.splice(index, 1);
    }
  }

  ngOnInit() {
  }

}
